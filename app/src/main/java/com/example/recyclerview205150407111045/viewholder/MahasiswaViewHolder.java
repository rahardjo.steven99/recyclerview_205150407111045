package com.example.recyclerview205150407111045.viewholder;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerview205150407111045.OnMahasiswaListener;
import com.example.recyclerview205150407111045.R;

public class MahasiswaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    TextView _tvNama, _tvNIM;
    ImageView _foto;
    Context _context;
    OnMahasiswaListener onMahasiswaListener;

    public MahasiswaViewHolder(@NonNull View itemView, OnMahasiswaListener onMahasiswaListener) {
        super(itemView);
        _context = itemView.getContext();
        _tvNama = itemView.findViewById(R.id.tv_nama);
        _tvNIM = itemView.findViewById(R.id.tv_NIM);
        _foto = itemView.findViewById(R.id.foto);
        this.onMahasiswaListener = onMahasiswaListener;

        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        onMahasiswaListener.onMahasiswaClick(getAdapterPosition());
    }

    public TextView get_tvNama() {
        return _tvNama;
    }

    public TextView get_tvNIM() {
        return _tvNIM;
    }

    public ImageView get_foto() {
        return _foto;
    }

    public Context get_context() {
        return _context;
    }
}

