package com.example.recyclerview205150407111045.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.lifecycle.GenericLifecycleObserver;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.example.recyclerview205150407111045.OnMahasiswaListener;
import com.example.recyclerview205150407111045.R;
import com.example.recyclerview205150407111045.model.Mahasiswa;
import com.example.recyclerview205150407111045.viewholder.MahasiswaViewHolder;

import java.util.ArrayList;

public class MahasiswaAdapter extends RecyclerView.Adapter<MahasiswaViewHolder> {

    private ArrayList<Mahasiswa> _mahasiswaList;
    private Context _context;
    private OnMahasiswaListener onMahasiswaListener;

    public MahasiswaAdapter(ArrayList<Mahasiswa> _mahasiswaList, Context _context, OnMahasiswaListener onMahasiswaListener) {
        this._mahasiswaList = _mahasiswaList;
        this._context = _context;
        this.onMahasiswaListener = onMahasiswaListener;
    }

    @NonNull
    @Override
    public MahasiswaViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_item, parent, false);
        MahasiswaViewHolder viewHolder = new MahasiswaViewHolder(v, onMahasiswaListener);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MahasiswaViewHolder holder, int position) {

        holder.get_tvNama().setText(_mahasiswaList.get(position).get_nama());
        holder.get_tvNIM().setText(_mahasiswaList.get(position).get_NIM());
        Glide.with(_context).load(_mahasiswaList.get(position).get_foto()).into(holder.get_foto());;

    }

    @Override
    public int getItemCount() {
        return _mahasiswaList.size();
    }
}
